with open('day3.txt') as file:
    directions = list(file.read())


class Santa(object):
    def __init__(self):
        self.current_x = 0
        self.current_y = 0
        self.visited = set()

    def follow_path(self, path):
        for direction in path:
            if direction == '>':
                self.current_x += 1
            elif direction == '<':
                self.current_x -= 1
            elif direction == '^':
                self.current_y += 1
            elif direction == 'v':
                self.current_y -= 1

            self.visited.add((self.current_x, self.current_y))


santa = Santa()

santa.follow_path(directions)

print("Visited {0}".format(len(santa.visited)))

santa1 = Santa()
santa2 = Santa()

santa1.follow_path(directions[0::2])
santa2.follow_path(directions[1::2])

total_visited = len(santa1.visited.union(santa2.visited))

print("Santa 1 and 2 visited {0}".format(total_visited))
