from re import search

with open('day5.txt') as file:
    inputs = file.readlines()

print("Part 1:")
print(len([x for x in inputs
           if search("((a|e|i|o|u).*){3,}", x)
           and search("(\w)\\1", x)
           and not search("ab|cd|pq|xy", x)]))

print("Part 2:")
print(len([x for x in inputs
           if search("(\w\w).*\\1", x)
           and search("(\w).\\1", x)]))
