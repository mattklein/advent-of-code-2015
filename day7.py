import re

operations = {
    'RSHIFT': lambda a, b: a >> b,
    'LSHIFT': lambda a, b: a << b,
    'OR': lambda a, b: a | b,
    'AND': lambda a, b: a & b,
    'NOT': lambda a: ~a & 0xFFFF,
}


def evaluate(value, wires) -> int:
    if value.isdigit():
        return int(value)

    if isinstance(wires[value], int):
        return wires[value]

    tokens = re.findall("([^\s]+)", wires[value])

    wires[value] = {
        1: lambda a: evaluate(a, wires),
        2: lambda op, a: operations[op](evaluate(a, wires)),
        3: lambda a, op, b: operations[op](evaluate(a, wires), evaluate(b, wires))
    }[len(tokens)](*tokens)

    return wires[value]


# parse the instructions and build a composition for evaluating the output
with open('day7.txt') as file:
    wires = {k: v for v, k in re.findall("(.*) -> ([^\s]*)", file.read())}

    part1 = evaluate("a", wires.copy())
    print(part1)

    wires["b"] = str(part1)
    part2 = evaluate("a", wires.copy())
    print(part2)
