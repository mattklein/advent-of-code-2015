with open('day1.txt') as file:
    elements = file.read()

result = 0
index = 1

for element in elements:
    if element == '(':
        result += 1
    elif element == ')':
        result -= 1

    if result == -1:
        print("Entered the basement on level {0}".format(index))

    index += 1

print("The result is: {0}".format(result))
