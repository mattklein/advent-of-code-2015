with open('day2.txt') as file:
    orders = file.readlines()

total_square_feet_of_wrapping_paper = 0
total_square_feet_of_ribbon = 0

for order in orders:
    (l, w, h) = map(int, order.split('x'))

    lw = l * w
    wh = w * h
    hl = h * l

    surface_area = 2 * lw + 2 * wh + 2 * hl
    slack = min(lw, wh, hl)

    total_area = surface_area + slack

    smallest_perimeter = min(2 * (l + w), 2 * (w + h), 2 * (h + l))

    bow_area = l * w * h

    total_square_feet_of_wrapping_paper += total_area
    total_square_feet_of_ribbon += smallest_perimeter + bow_area

print("Total square feet of wrapping paper: {0}".format(total_square_feet_of_wrapping_paper))
print("Total square feet of ribbon: {0}".format(total_square_feet_of_ribbon))
