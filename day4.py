import hashlib
import sys

with open('day4.txt') as file:
    secret_key = file.read()

found_with_5_zeroes = False
found_with_6_zeroes = False

for i in iter(range(0, sys.maxsize)):
    digest = hashlib.md5((secret_key + str(i)).encode('utf-8')).hexdigest()

    if digest.startswith("00000") and not found_with_5_zeroes:
        print("The lowest number with five zeroes is {0}".format(i))
        found_with_5_zeroes = True

    if digest.startswith("000000") and not found_with_6_zeroes:
        print("The lowest number with six zeroes is {0}".format(i))
        found_with_6_zeroes = True

    if found_with_5_zeroes and found_with_6_zeroes: break
