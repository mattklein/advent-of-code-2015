import re

from ast import literal_eval as make_tuple

with open('day6.txt') as source:
    instructions = source.readlines()

grid = [[0 for x in range(1000)] for y in range(1000)]


def foreach_in_range(x1, y1, x2, y2, delegate):
    """
    Given some intermediate range, applies the given delegate over each
    node in the grid for that range.
    """
    for i in iter(range(x1, x2 + 1)):
        for j in iter(range(y1, y2 + 1)):
            current = grid[i][j]
            grid[i][j] = delegate(current)


for instruction in instructions:
    matches = re.match("(turn off|turn on|toggle) ([0-9]+,[0-9]+) through ([0-9]+,[0-9]+)", instruction)

    mode = matches.group(1)

    (x1, y1) = make_tuple(matches.group(2))
    (x2, y2) = make_tuple(matches.group(3))

    if "turn on" in mode:
        foreach_in_range(x1, y1, x2, y2, lambda c: c + 1)

    if "turn off" in mode:
        foreach_in_range(x1, y1, x2, y2, lambda c: max(c - 1, 0))

    if "toggle" in mode:
        foreach_in_range(x1, y1, x2, y2, lambda c: c + 2)

active = 0
for i in iter(range(0, 1000)):
    for j in iter(range(0, 1000)):
        active += grid[i][j]

print("Active lights: {}".format(active))
